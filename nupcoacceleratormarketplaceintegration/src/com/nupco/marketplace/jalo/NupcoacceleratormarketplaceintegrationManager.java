/*
 *  
 * [y] hybris Platform
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.nupco.marketplace.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.nupco.marketplace.constants.NupcoacceleratormarketplaceintegrationConstants;
import org.apache.log4j.Logger;

@SuppressWarnings("PMD")
public class NupcoacceleratormarketplaceintegrationManager extends GeneratedNupcoacceleratormarketplaceintegrationManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( NupcoacceleratormarketplaceintegrationManager.class.getName() );
	
	public static final NupcoacceleratormarketplaceintegrationManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (NupcoacceleratormarketplaceintegrationManager) em.getExtension(NupcoacceleratormarketplaceintegrationConstants.EXTENSIONNAME);
	}
	
}
