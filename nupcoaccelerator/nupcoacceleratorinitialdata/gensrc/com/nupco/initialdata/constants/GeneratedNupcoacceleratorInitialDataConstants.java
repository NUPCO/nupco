/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Oct 4, 2018 11:16:03 PM                     ---
 * ----------------------------------------------------------------
 */
package com.nupco.initialdata.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedNupcoacceleratorInitialDataConstants
{
	public static final String EXTENSIONNAME = "nupcoacceleratorinitialdata";
	
	protected GeneratedNupcoacceleratorInitialDataConstants()
	{
		// private constructor
	}
	
	
}
